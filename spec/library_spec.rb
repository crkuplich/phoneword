#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

require 'minitest/autorun'

describe 'Phoneword library usage' do
  require 'phoneword'

  describe 'when input factory method is used' do
    it 'must return a Keypad instance with the given input' do
      keypad = Phoneword::Keypad.input('something')
      keypad.must_be_instance_of(Phoneword::Keypad)
      keypad.input.must_equal('something')
    end

    describe 'when number method is chained with the input factory' do
      it 'must return the number for the given input' do
        Phoneword::Keypad.input('bosco').number.must_equal('26726')
      end
    end

    describe 'when words method is chained with the input factory' do
      it 'must return words for the given input' do
        Phoneword::Keypad.input('26726').words.must_include('bosco')
      end
    end
  end
end
