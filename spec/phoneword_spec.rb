#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

require 'minitest/autorun'
require_relative 'helpers/spec_helper'

HELP_MESSAGE_PATTERN = /^Usage: phoneword <options> <args>/

describe Phoneword do
  include SpecHelper

  describe "when passed 'number' option" do
    describe 'and a word with lowercase letters is given' do
      it 'must output the correspondent number' do
        phoneword('-n', 'password').must_output "72779673\n"
        phoneword('--number', 'password').must_output "72779673\n"
      end
    end

    describe 'and a word with uppercase letters is given' do
      it 'must output the correspondent number' do
        phoneword('-n', 'PASSWORD').must_output "72779673\n"
        phoneword('-n', 'Password').must_output "72779673\n"
        phoneword('-n', 'PaSsWord').must_output "72779673\n"
      end
    end

    describe 'and two or more words separated by spaces are given' do
      it 'must output an error message' do
        phoneword('-n', 'two words').must_output nil, /Invalid character: ' '/
        phoneword('-n', 'one two three').must_output nil,
                                                     /Invalid character: ' '/
      end
    end

    describe 'and two or more arguments are given' do
      it 'must output a number for each argument' do
        phoneword('-n', 'bosco', 'password').must_output "26726\n72779673\n"
        phoneword('--number', 'bosco', 'password').must_output(
          "26726\n72779673\n")
      end
    end

    describe 'and a number is given' do
      it 'must output the number as itself' do
        phoneword('-n', '0123456789').must_output "0123456789\n"
      end
    end

    describe 'and digits among letters are given' do
      it 'must output the digits as itself' do
        phoneword('-n', '0001').must_output "0001\n"
        phoneword('-n', '0010').must_output "0010\n"
        phoneword('-n', '00seven').must_output "0073836\n"
        phoneword('-n', '10words').must_output "1096737\n"
        phoneword('-n', '10wOrDs').must_output "1096737\n"
        phoneword('-n', '1234five6').must_output "123434836\n"
      end
    end

    describe 'and special characters are given' do
      it 'must output an error message' do
        phoneword('-n', '!@$%').must_output nil, /Invalid character: '!'/
      end
    end

    describe 'and no argument is given' do
      it 'must output the help message' do
        phoneword('-n').must_output HELP_MESSAGE_PATTERN
        phoneword('--number').must_output HELP_MESSAGE_PATTERN
      end
    end
  end

  describe "when passed 'words' option" do
    describe 'and a number argument is given' do
      it 'must output the possible words for the number' do
        phoneword('-w', '72779673').must_output(/password/)
        phoneword('--words', '72779673').must_output(/password/)
      end
    end

    describe 'and a word is given' do
      it 'must output the same letters of the word' do
        phoneword('-w', 'test').must_output(/test/)
        phoneword('--words', 'test').must_output(/test/)
      end
    end

    describe 'and letters among digits are given' do
      it 'must output the letters as itself' do
        phoneword('-w', 'testing123').must_output(/testing1ad/)
      end
    end

    describe 'and special characters are given' do
      it 'must output an error message' do
        phoneword('-w', '!@$%').must_output nil, /Invalid character: '!'/
      end
    end

    describe 'and two or more arguments are given' do
      it 'must output a number for each argument' do
        phoneword('-w', '26726', '72779673').must_output(/bosco.*password/m)
        phoneword('--words', '26726', '72779673').must_output(
          /bosco.*password/m)
      end
    end

    describe 'and no argument is given' do
      it 'must output the help message' do
        phoneword('-w').must_output HELP_MESSAGE_PATTERN
        phoneword('--words').must_output HELP_MESSAGE_PATTERN
      end
    end
  end

  describe "when 'number' and 'words' options are passed" do
    it 'must output number and words for each argument' do
      phoneword('-n', '-w', 'bosco', 'password').must_output(
        "26726\nbosco\n72779673\npassword\n")
      phoneword('-w', '-n', 'bosco', 'password').must_output(
        "26726\nbosco\n72779673\npassword\n")
    end
  end

  describe 'when run with no options or arguments' do
    it 'must output the help message' do
      phoneword.must_output HELP_MESSAGE_PATTERN
    end
  end

  describe "when passed 'help' option" do
    it 'must output a help message' do
      phoneword('-h').must_output HELP_MESSAGE_PATTERN
      phoneword('--help').must_output HELP_MESSAGE_PATTERN
    end
  end

  describe "when passed 'version' option" do
    it 'must output Phoneword version' do
      phoneword('-v').must_output(/^phoneword version \d{1,2}\.\d*\.\d*.*$/)
      phoneword('--version').must_output(
        /^phoneword version \d{1,2}\.\d*\.\d*.*$/)
    end
  end

  describe 'when passed an invalid option' do
    it 'must output an error message' do
      phoneword('--invalid').must_output nil, /Invalid option: --invalid/
      phoneword('-x').must_output nil, /Invalid option: -x/
      phoneword('-x -y').must_output nil, /Invalid option: -x/
    end
  end
end
