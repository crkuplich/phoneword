#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

require 'optparse'
require 'phoneword/keypad'
require 'phoneword/version'

module Phoneword
  ##
  # Runner class to run Phoneword with command-line options.
  class Runner
    ##
    # Runs Phoneword passing +args+ containing command-line options. Please, run
    # <tt>phoneword -h</tt> in terminal to see which +args+ are accepted.
    def self.run(args)
      args.push('-h') if args.empty?
      begin
        parse_options(args)
        process(args)
      rescue OptionParser::ParseError, ArgumentError => e
        abort show_error_message(e.message)
      end
    end

    class << self
      private

      def parse_options(args)
        @options = {}
        opt_parser = OptionParser.new do |parser|
          help_message_header(parser)
          number_option(parser)
          words_option(parser)
          help_option(parser)
          version_option(parser)
        end
        opt_parser.parse!(args)
        show_help(opt_parser) if args.empty?
      end

      def process(args)
        args.each do |arg|
          keypad = Keypad.input(arg)
          puts keypad.number if @options[:number]
          puts keypad.words if @options[:words]
        end
      end

      def help_message_header(parser)
        parser.banner = 'Usage: phoneword <options> <args>'
        parser.separator ''
        parser.separator 'Options:'
      end

      def number_option(parser)
        @options[:number] = false
        parser.on('-n', '--number',
                  'Show the correspondent number for each',
                  '  argument in <args>') do
          @options[:number] = true
        end
      end

      def words_option(parser)
        @options[:words] = false
        parser.on('-w', '--words',
                  'Show possible words for each argument',
                  '  in <args>') do
          @options[:words] = true
        end
      end

      def help_option(parser)
        parser.on('-h', '--help', 'Show this message and exit') do
          show_help(parser)
        end
      end

      def version_option(parser)
        parser.on('-v', '--version', 'Show version and exit') do
          show_version
        end
      end

      def show_help(options)
        puts options
        exit
      end

      def show_version
        puts "phoneword version #{VERSION}"
        exit
      end

      def show_error_message(msg)
        msg[0] = msg[0].upcase
        abort msg
      end
    end
  end
end
