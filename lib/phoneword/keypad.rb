#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

module Phoneword
  ##
  # Represents a telephone keypad.
  class Keypad
    CHARS = { # :nodoc:
      '0' => ['0'],
      '1' => ['1'],
      '2' => %w(a b c),
      '3' => %w(d e f),
      '4' => %w(g h i),
      '5' => %w(j k l),
      '6' => %w(m n o),
      '7' => %w(p q r s),
      '8' => %w(t u v),
      '9' => %w(w x y z)
    }.freeze

    ##
    # Original input.
    attr_reader :input

    ##
    # Creates a Keypad instance given the +input+. Raises an +ArgumentError+ if
    # the +input+ contains a non-word character.
    def self.input(input)
      keypad = Keypad.new
      keypad.input = input
      keypad
    end

    ##
    # Creates a Keypad instance.
    def initialize
      @digit = {}
      CHARS.invert.each do |chrs, num|
        chrs.each { |c| @digit[c] = num }
      end
      @input = ''
    end

    ##
    # Inputs into the keypad. Raises an +ArgumentError+ if the +input+ contains
    # a non-word character.
    def input=(input)
      fail ArgumentError,
           "Invalid character: '#{Regexp.last_match(1)}'" if input =~ /(\W)/
      @input = input unless input.nil?
    end

    ##
    # Retrieves the number corresponding to the input. The number is composed of
    # digits associated to each character of the input mapped by the keypad.
    # Digits in the original input are repassed directly to the output of this
    # method.
    def number
      input.downcase.gsub(/[01a-z]/) { |c| @digit[c] }
    end

    ##
    # Retrieves all possible words combining the characters corresponding to
    # each digit of the input. Characters in the original input are repassed
    # directly to the output of this method.
    def words
      return [] if input.empty?
      words_for(input.downcase)
    end

    private

    def words_for(number)
      if number.size == 1
        return [number] if number =~ /[a-z]/
        CHARS[number]
      else
        words = []
        words_for(number[0]).each do |c1|
          words_for(number[1..-1]).each { |c2| words << c1 + c2 }
        end
        words
      end
    end
  end
end
