# Phoneword

Phoneword is a mnemonic/number converter based on a telephone keypad.

## Installation

Phoneword requires Ruby 2.1.8 or newer to be installed.

Clone the Phoneword repository:

    $ git clone https://gitlab.com/crkuplich/phoneword.git

Change to the created directory and use `gem` to build and install the Phoneword
gem:

    $ cd phoneword/
    $ gem build phoneword.gemspec
    $ gem install phoneword-<version>.gem

## Usage

### As a command-line application

If you want to get the corresponding number of a word input:

    $ phoneword -n bosco
    26726

If you want to get the possible words for a number input:

    $ phoneword -w 26726
    ...
    bosco
    ...

Type `phoneword -h` for a more detailed help message with all possible options.

### As a library

```ruby
require 'phoneword'

# Get the number of a word input
Phoneword::Keypad.input("bosco").number
# => "26726"

# Get "words" (all possible combination of letters) for a number input
Phoneword::Keypad.input("26726").words
# => ["ampam", "ampan", ..., "bosco", ...]
```

## Contributing

Bug reports and merge requests are welcome on GitLab at
https://gitlab.com/crkuplich/phoneword.

## License

The gem is available as free software under the terms of the
[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).
