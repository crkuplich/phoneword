#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

require 'minitest/autorun'
require 'phoneword/keypad'

##
# Keypad unit tests.
class TestKeypad < Minitest::Test
  attr_reader :keypad

  def setup
    @keypad = Phoneword::Keypad.new
  end

  def test_input
    # a keypad input must be empty by default
    assert_equal '', keypad.input

    keypad.input = 'abc'
    assert_equal 'abc', keypad.input

    keypad.input = '123'
    assert_equal '123', keypad.input
  end

  def test_empty_input
    keypad.input = ''
    assert_empty keypad.input
  end

  def test_special_characters_as_input
    keypad.input = '!@#$%'
    flunk 'ArgumentError expected but nothing was raised.'
  rescue ArgumentError => e
    pass # raised ArgumentError
    assert_equal "Invalid character: '!'", e.message
  end

  def test_get_correspondent_number_of_the_input
    keypad.input = 'bosco'
    assert_equal '26726', keypad.number

    keypad.input = 'testing'
    assert_equal '8378464', keypad.number
  end

  def test_convert_all_valid_chars_to_numbers
    expected = '0122233344455566677778889999'

    chars = '01'
    ('a'..'z').each { |c| chars << c }
    keypad.input = chars
    assert_equal expected, keypad.number

    chars = '01'
    ('A'..'Z').each { |c| chars << c }
    keypad.input = chars
    assert_equal expected, keypad.number
  end

  def test_get_correspondent_number_of_an_input_with_digits
    keypad.input = '123456'
    assert_equal '123456', keypad.number

    keypad.input = 'bosco2016'
    assert_equal '267262016', keypad.number
  end

  def test_get_correspondent_number_of_an_empty_input
    keypad.input = ''
    assert_equal '', keypad.number

    keypad.input = nil
    assert_equal '', keypad.number

    # a keypad input is empty by default
    new_keypad = Phoneword::Keypad.new
    assert_equal '', new_keypad.number
  end

  def test_get_words_for_the_input
    keypad.input = '1'
    assert_equal ['1'], keypad.words

    keypad.input = '2'
    assert_equal %w(a b c), keypad.words

    keypad.input = '26726'
    assert_includes keypad.words, 'bosco'
  end

  def test_get_words_for_empty_input
    assert_empty keypad.words
  end

  def test_get_words_for_an_input_with_letters
    keypad.input = 'abcdef'
    assert_equal ['abcdef'], keypad.words

    keypad.input = 'ABCDEF'
    assert_equal ['abcdef'], keypad.words

    keypad.input = 'bosco2016'
    assert_includes keypad.words, 'boscoa01m'
  end

  def test_get_keypad_from_input_factory_method
    keypad_from_input = Phoneword::Keypad.input('something')
    assert_instance_of Phoneword::Keypad, keypad_from_input
    assert_equal 'something', keypad_from_input.input
  end

  def test_factory_method_with_invalid_input
    assert_raises(ArgumentError) { Phoneword::Keypad.input("!@\#$%") }
  end

  def test_factory_method_with_nil_input
    keypad_from_input = Phoneword::Keypad.input(nil)
    assert_equal '', keypad_from_input.input
  end

  def test_factory_method_with_empty_input
    keypad_from_input = Phoneword::Keypad.input('')
    assert_equal '', keypad_from_input.input
  end
end
