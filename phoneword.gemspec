#--
# Copyright 2016 Cassiano Rocha Kuplich
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#++

require_relative 'lib/phoneword/version'

Gem::Specification.new do |s|
  s.name = 'phoneword'
  s.version = Phoneword::VERSION
  s.summary = 'A mnemonic/number converter based on a telephone keypad.'
  s.description =
    'Phoneword is a mnemonic/number converter based on a telephone keypad.'
  s.authors = ['Cassiano Kuplich']
  s.email = 'crkuplich@openmailbox.org'
  s.files = `git ls-files -z`.split("\x0")
  s.executables = ['phoneword']
  s.homepage = 'https://gitlab.com/crkuplich/phoneword'
  s.license = 'Apache-2.0'
  s.required_ruby_version = '>= 2.1.8'
  s.add_development_dependency 'minitest', '~> 5.0'
  s.add_development_dependency 'rake', '~> 10.2'
  s.add_development_dependency 'rdoc', '~> 4.1'
  s.add_development_dependency 'rubocop', '~> 0.36'
end
